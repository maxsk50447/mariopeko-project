var Block = cc.Sprite.extend({
    ctor: function() {
        this._super();
        this.initWithFile('res/images/block.png');
    },
    hit : function(player) {
        if (player.getPositionY()>=this.getPositionY()-60
            && player.getPositionY()<=this.getPositionY()+60
            && player.getPositionX()<this.getPositionX()+60
            && player.getPositionX()>this.getPositionX()-60) {
            player.freefall = false;
            player.vy = 0;
            return true;
        }
        return false;
    }
});
	