 var Apple = cc.Sprite.extend({
    ctor: function (){
        this._super();
        this.initWithFile('res/images/apple.png');
    },
     randomPosition: function(){
         this.setPositionX(this.getRandomIntInclusive(100,600));
         this.setPositionY(this.getRandomIntInclusive(400,450));
     },
     getRandomIntInclusive :function (min, max){
        return Math.floor(Math.random()* (max - min + 1)) + min;
    },
     closeTo: function( obj ) {
         var myPos = this.getPosition();
         var oPos = obj.getPosition();

         return ( ( Math.abs( myPos.x - oPos.x ) <= 40 ) &&
         ( Math.abs( myPos.y - oPos.y ) <= 40 ) );
     }
 });