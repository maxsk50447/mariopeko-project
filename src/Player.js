var Player = cc.Sprite.extend({
    ctor: function() {
        this._super();
        this.initWithFile('res/images/bird_right.png');
        this.vy = 0;
        this.vx = 0;
        this.freefall = true;
    },
    update : function(dt) {
        if (this.getPositionX()>800) {
            this.setPositionX(0);
        }
        if (this.getPositionX()<0) {
            this.setPositionX(800);
        }
        if (this.freefall) {
            var pos = this.getPosition();
            this.setPosition(new cc.Point(pos.x+this.vx, pos.y + this.vy));
            this.vy += -1;
        } else {
            this.setPositionX(this.getPositionX()+this.vx);
        }
    },
    jump: function() {
        this.vy = 20;
        this.freefall = true;
    },
    moveLeft: function() {
        this.vx = -5;
        this.setTexture('res/images/bird_left.png');
        this.freefall = true;
    },
    moveRight: function() {
        this.vx = 5;
        this.setTexture('res/images/bird_right.png');
        this.freefall = true;
    },
});