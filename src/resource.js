var res = {
    bird_right_png : "res/images/bird_right.png",
    bird_left_png : "res/images/bird_left.png",
    block_png : "res/images/block.png",
    background_png : "res/images/background.png",
    menubackground_png : "res/images/menubackground.png",
    menulogo_png : "res/images/logo.png",
    playbutton_png: "res/images/playbutton.png",
    playbuttonclick_png: "res/images/playbuttonclick.png",
    instructionbutton_png: "res/images/instructionbutton.png",
    instructionbuttonclick_png: "res/images/instructionbuttonclick.png",
    instructionbackground_png: "res/images/instructionbackground.png",
    mainbgm_mp3: "res/effects/mainbgm.mp3",
    gameoverbackground_png: "res/images/gameoverbackground.png"
};

var g_resources = [];
for (var i in res) {
    g_resources.push(res[i]);
}
