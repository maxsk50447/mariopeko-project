var GameOver = cc.LayerColor.extend({
    ctor: function() {
        this._super(new cc.Color(0, 0, 0, 0));
        this.init();

    },
    init: function () {
        this.gameoverBackground = new cc.Sprite(res.gameoverbackground_png);
        this.gameoverBackground.setPosition(new cc.Point(screenWidth / 2,screenHeight / 2));
        this.addChild(this.gameoverBackground);


        this.score = score;
        this.scoreLabel = cc.LabelTTF.create( this.score + "", 'Tahoma', 60 );
        this.scoreLabel.setPosition( new cc.Point( 675, 300 ) );
        this.addChild( this.scoreLabel );
        cc.audioEngine.playMusic(res.mainbgm_mp3,true);

        this.addKeyboardHandlers();

        return true;
    },
    addKeyboardHandlers : function(){
        var self = this;
        this.score = 0;
        cc.eventManager.addListener({
            event: cc.EventListener.KEYBOARD,
            onKeyPressed : function (keyCode, event){
                self.onKeyDown(keyCode, event);
            },
            onKeyReleased: function (keyCode, event){
                
            }
        }, this);
    },
    onKeyDown: function (keyCode, event){
        if (keyCode == cc.KEY.space){
            location.reload();
        }
    }
});


var GameEnd = cc.Scene.extend({
    onEnter : function() {
        this._super();
        var layer = new GameOver();
        layer.init();
        this.addChild(layer);
    }
});
