var score = 0;
var GameLayer = cc.LayerColor.extend({
        init : function() {
            this._super(new cc.Color(127, 127, 127, 255));
            this.setPosition(new cc.Point(0, 0));
            this.background = new cc.Sprite(res.background_png);
            this.background.setPosition(new cc.Point(screenWidth/2,screenHeight/2));
            this.addChild(this.background);
            this.player = new Player();
            this.player.setPosition(new cc.Point(100, 150));
            this.addChild(this.player, 1);


            this.blockFirstRow = [new Block(), new Block(), new Block(), new Block()];
            for(var a = 0; a < this.blockFirstRow.length; a++){
                this.blockFirstRow[a].setPositionX(100 + (a * 50));
                this.blockFirstRow[a].setPositionY(80);
                this.addChild(this.blockFirstRow[a]);
            }
            this.blockSecondRow = [new Block(), new Block(), new Block(), new Block()];
            for(var b = 0; b < this.blockSecondRow.length;b++) {
                this.blockSecondRow[b].setPositionX(325 + (b * 50));
                this.blockSecondRow[b].setPositionY(250);
                this.addChild(this.blockSecondRow[b]);
            }
            this.blockThirdRow = [new Block(), new Block(), new  Block(), new Block()];
            for(var c = 0; c < this.blockThirdRow.length; c++){
                this.blockThirdRow[c].setPositionX(550 + (c * 50));
                this.blockThirdRow[c].setPositionY(80);
                this.addChild(this.blockThirdRow[c]);
            }

            this.apple = new Apple();
            this.addChild(this.apple);
            this.apple.randomPosition();

            this.ghost = [new Ghost(), new Ghost()];
            for(var d = 0; d < this.ghost.length; d++) {
                this.ghost[d].randomPosition();
                this.addChild(this.ghost[d]);
                this.ghost[d].scheduleUpdate();
            }

            this.scoreLabel = cc.LabelTTF.create( "Score: 0", 'Tahoma', 40 );
            this.scoreLabel.setPosition( new cc.Point( 650, 550 ) );
            this.addChild( this.scoreLabel );

            this.gameOverLabel = cc.LabelTTF.create( '', 'Tahoma', 80);
            this.gameOverLabel.setPosition(new cc.Point(400,400));
            this.addChild( this.gameOverLabel );

            this.player.scheduleUpdate();

            this.addKeyboardHandlers();
            this.scheduleUpdate();



            return true;
        },
        update : function(dt) {
            this.checkBlock();
            this.scoreUpdate()
            for(var l = 0; l < this.ghost.length ; l++) {
                if (this.ghost[l].closeTo(this.player) || this.player.getPositionY() < 0) {
                    cc.director.runScene(new GameOver());
                }
            }
        },

        checkBlock : function(){
            for (var i = 0; i < this.blockFirstRow.length; i++) {
                if (this.blockFirstRow[i].hit(this.player)) {
                    break;
                }
            }
            for (var j = 0; j < this.blockSecondRow.length; j++) {
                if (this.blockSecondRow[j].hit(this.player)) {
                    break;
                }
            }
            for (var k = 0; k < this.blockThirdRow.length; k++) {
                if (this.blockThirdRow[k].hit(this.player)) {
                    break;
                }
            }
        },




        addKeyboardHandlers : function() {
            var self = this;
            cc.eventManager.addListener({
                event : cc.EventListener.KEYBOARD,
                onKeyPressed : function(keyCode, event) {
                    self.onKeyDown(keyCode, event);
                },
                onKeyReleased : function(keyCode, event) {
                    self.onKeyUp(keyCode, event);
                }
            }, this);
        },
        onKeyDown : function(keyCode, event) {
            if (keyCode == cc.KEY.up) {
                if (!this.player.freefall) {
                    this.player.jump();
                }
            } if (keyCode == cc.KEY.right) {
                this.player.moveRight();
            } if (keyCode == cc.KEY.left) {
                this.player.moveLeft();
            }
        },
        onKeyUp : function(keyCode, event) {
            if (keyCode!=cc.KEY.up)
                this.player.vx = 0;
        },
        scoreUpdate : function(){
            if ( this.apple.closeTo( this.player ) ) {
                this.apple.randomPosition();
                score += 10;
                this.scoreLabel.setString( "Score: " + score );
            }
        },


});

var StartScene = cc.Scene.extend({
    onEnter : function() {
        this._super();
        var layer = new GameLayer();
        layer.init();
        this.addChild(layer);
    }
});
