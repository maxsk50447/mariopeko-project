var GameInstruction = cc.LayerColor.extend({
    ctor: function() {
        this._super(new cc.Color(127, 127, 127, 255));
        this.init();

    },
    init: function () {
        this.instructionBackground = new cc.Sprite(res.instructionbackground_png);
        this.instructionBackground.setPosition(new cc.Point(screenWidth / 2,screenHeight / 2));
        this.addChild(this.instructionBackground);


        this.playBtn = new cc.MenuItemImage(res.playbutton_png, res.playbuttonclick_png, playGame);
        this.playBtn.setPosition(new cc.Point(700,35));

        this.menu = new cc.Menu(this.playBtn);
        this.menu.setPosition(0,0);
        this.addChild(this.menu);
    
        return true;
    },
});

var playGame = function() {
    cc.director.runScene(new StartScene());
}



GameInstruction.scene = function() {
    var scene = new cc.Scene();
    var layer = new GameInstruction();
    scene.addChild(layer);
    return scene;
};
