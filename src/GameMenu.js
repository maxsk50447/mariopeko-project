var director = cc.Director();
var GameMenu = cc.LayerColor.extend({
    ctor: function() {
        this._super(new cc.Color(127, 127, 127, 255));
        this.init();

    },
    init: function () {
        this.menuBackground = new cc.Sprite(res.menubackground_png);
        this.menuBackground.setPosition(new cc.Point(screenWidth / 2, screenHeight / 2));
        this.addChild(this.menuBackground);

        this.logo = new cc.Sprite(res.menulogo_png)
        this.logo.setPosition(new cc.Point(screenWidth / 2, (screenHeight / 2) + 100));
        this.addChild(this.logo);

        this.playBtn         = new cc.MenuItemImage(res.playbutton_png, res.playbuttonclick_png, playGame);
        this.instructionBtn  = new cc.MenuItemImage(res.instructionbutton_png, res.instructionbuttonclick_png, instruction);

        this.playBtn.setPosition(new cc.Point(screenWidth / 2, ((screenHeight / 2) - 50)));
        this.instructionBtn.setPosition(new cc.Point(screenWidth / 2, ((screenHeight / 2) - 150)));

        this.menu = new cc.Menu(this.playBtn, this.instructionBtn);
        this.menu.setPosition(0,0);
        this.addChild(this.menu);

        cc.audioEngine.playMusic(res.mainbgm_mp3,true);

        return true;
    },
});

var playGame = function() {
    cc.director.runScene(new StartScene());
}

var instruction = function() {
    cc.director.runScene(GameInstruction.scene());
}


GameMenu.scene = function() {
    var scene = new cc.Scene();
    var layer = new GameMenu();
    scene.addChild(layer);
    return scene;
};
