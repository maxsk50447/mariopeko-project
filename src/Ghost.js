var Ghost = cc.Sprite.extend({
    ctor: function(){
        this._super();
        this.vy = 0;
        this.freefall = true;
    },
    update: function(dt) {
        this.randomColor();
        if (this.getPositionY() < 0) {
            this.randomPosition();
        }
        if (this.freefall) {
            var pos = this.getPosition();
            this.setPositionY( pos.y + this.vy);
            if(this.vy >= -10){this.vy += -1;}
        }
    },
    randomColor: function() {
        var color = Math.floor(Math.random() * 5) + 1;
        if (color == 1) this.initWithFile('res/images/red.png');
        else if (color == 2) this.initWithFile('res/images/blue.png');
        else if (color == 3) this.initWithFile('res/images/green.png');
        else if (color == 4) this.initWithFile('res/images/pink.png');
        else if (color == 5) this.initWithFile('res/images/sky.png');
    },
    randomPosition: function(){
        this.setPositionY( 700);
        this.setPositionX(Math.random() * 800);
    },
    getRandomIntInclusive :function (min, max){
        return Math.floor(Math.random() * (max - min + 1)) + min;
    },
    closeTo: function( obj ) {
        var myPos = this.getPosition();
        var oPos = obj.getPosition();

        return ( ( Math.abs( myPos.x - oPos.x ) <= 40 ) &&
        ( Math.abs( myPos.y - oPos.y ) <= 40 ) );
    }
});

